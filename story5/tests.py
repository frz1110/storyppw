from django.test import TestCase, Client
from .models import Jadwal
from .views import form_matkul, savejadwal, delete, detail, jadwal
from django.urls import resolve
 

# Create your tests here.
class test_form(TestCase):
    def test_url(self):
        response = Client().get('/form')
        self.assertEqual(response.status_code, 200)
        
    def test_templates(self):
        response = Client().get('/form')
        self.assertTemplateUsed(response, 'jadwal/matkul.html')
        html = response.content.decode('utf8')
        self.assertIn('<button  type="submit" class="btn btn-secondary"  style="background-color:#87a96b;" >Tambah</button>', html)

    def test_database(self):
        Jadwal.objects.create(nama_matkul="PPW", dosen="siapa", jumlah_sks=3,tahun_ajaran="2020", ruang_kelas="A", deskripsi_matkul="jahd")
        hitung_banyak_jadwal = Jadwal.objects.all().count()
        self.assertEqual(hitung_banyak_jadwal, 1)

    def test_views_funct(self):
        found = resolve('/form')
        self.assertEqual(found.func, form_matkul) 

        found1 = resolve('/Savejadwal')
        self.assertEqual(found1.func, savejadwal) 

class test_delete(TestCase):
 
    def test_redirect(self):
        matkul = Jadwal.objects.create(nama_matkul="PPW", dosen="siapa", jumlah_sks=3,tahun_ajaran="2020", ruang_kelas="A", deskripsi_matkul="jahd")
        Jadwal.objects.get(pk = matkul.pk).delete()
        hitung_banyak_jadwal = Jadwal.objects.all().count()
        self.assertEqual(hitung_banyak_jadwal, 0)
        
    def test_views(self):
        found = resolve('/delete/1')
        self.assertEqual(found.func, delete) 

class test_detail(TestCase):

    def test_views(self):
        found = resolve('/detail/1')
        self.assertEqual(found.func, detail) 
       



    
        

        

    
        
  

# Create your tests here.

