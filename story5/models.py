from django.db import models

# Create your models here.
class Jadwal(models.Model):
    nama_matkul = models.CharField(max_length = 50)
    dosen = models.CharField(max_length = 50)
    jumlah_sks = models.IntegerField(null = True)
    tahun_ajaran = models.CharField(max_length = 15)
    ruang_kelas = models.CharField(max_length = 20,default='none')
    deskripsi_matkul = models.CharField(max_length = 300)

