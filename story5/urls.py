from django.urls import path
from . import views

app_name = 'story5'

urlpatterns = [
    path('form', views.form_matkul, name='form'),
    path('Savejadwal', views.savejadwal, name='save'),
    path('jadwal', views.jadwal, name='jadwal'),
    path('delete/<int:pk>', views.delete, name='delete'),
    path('detail/<int:pk>', views.detail, name='detail'),
    
]
