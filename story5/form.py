from django import forms
from .models import Jadwal

class FormMatkul(forms.ModelForm):
    class Meta:
        model = Jadwal
        fields = "__all__"
    
    nama_matkul = forms.CharField(label = "Mata Kuliah", required = True, max_length = 25)
    dosen = forms.CharField(label = "Dosen",required = True, max_length = 20)
    jumlah_sks = forms.IntegerField(label = "Jumlah SKS", required = True)
    tahun_ajaran = forms.CharField(label = "Tahun Ajaran", required = True)
    ruang_kelas = forms.CharField(label = "Ruang Kelas", required = True)
    deskripsi_matkul = forms.CharField(label = "Deskripsi",required = False, widget=forms.Textarea)
    
