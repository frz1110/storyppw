from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .form import FormMatkul
from .models import Jadwal

# Create your views here.
def form_matkul(request):
    form = FormMatkul
    response = {"form":form, "title":"Tambah Jadwal"}
    return render(request,"jadwal/matkul.html",response)

def savejadwal(request):
    form = FormMatkul(request.POST)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
    return redirect("/form")
        
def delete(request, pk):
    Jadwal.objects.get(pk = pk).delete()
    return redirect("/jadwal")

def jadwal(request):
    jadwal = Jadwal.objects.all()
    response = {"jadwal":jadwal, "title":"Jadwal Kuliah"}
    return render(request,"jadwal/jadwal.html",response)

def detail(request, pk):
    detail = Jadwal.objects.get(pk=pk)
    response = {"jadwal":detail, "title":"detail"}
    return render(request,"jadwal/detail.html",response)