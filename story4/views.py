from django.shortcuts import render


def home(request):
    return render(request, 'main/home.html', {'title':'Home'})

def login(request):
    return render(request, 'main/login.html', {'title':'Login'})