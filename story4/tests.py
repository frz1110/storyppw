from django.test import TestCase, Client
from .views import home
from django.urls import resolve
 

# Create your tests here.
class test_story4(TestCase):
    def test_url(self):
        response = Client().get('/home')
        self.assertEqual(response.status_code, 200)
        
        
    def test_templates(self):
        response = Client().get('/home')
        self.assertTemplateUsed(response, 'main/home.html')



    def test_views_funct(self):
        found = resolve('/home')
        self.assertEqual(found.func, home) 

       


