from django.urls import path

from . import views

app_name = 'story4'

urlpatterns = [
    path('', views.home, name='awal'),
    path('home', views.home, name='home'),
    path('login', views.login, name='login'),
   
]
