from django.test import TestCase, Client
from .views import profil
from django.urls import resolve
 

# Create your tests here.
class test_form(TestCase):
    def test_url(self):
        response = Client().get('/story1')
        self.assertEqual(response.status_code, 200)
        
    def test_templates(self):
        response = Client().get('/story1')
        self.assertTemplateUsed(response, 'story1/story1.html')
        html = response.content.decode('utf8')
        self.assertIn('Welcome To My Website', html)


    def test_views_funct(self):
        found = resolve('/story1')
        self.assertEqual(found.func, profil) 

     
