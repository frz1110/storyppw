from django.test import TestCase, Client
from django.urls import resolve
from .views import profil

# Create your tests here.
class accordion(TestCase):
    def test_url_ada(self):
        response = Client().get('/profil')
        self.assertEqual(response.status_code, 200)
    def test_templates_yang_digunakan(self):
        response = Client().get('/profil')
        self.assertTemplateUsed(response, 'accordion.html')
    def test_views(self):
        found = resolve('/profil')
        self.assertEqual(found.func, profil)