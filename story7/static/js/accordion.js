

$(document).ready(function() {
    
    var title = $(".title");
    var acr = $(".accordion");
    var up = $(".up");
    var down = $(".down");
    var i;
    
    for (i = 0; i < title.length; i++) {
        
      $(title[i]).click(function() {
        $(this).parent().next().toggle();
      });
    } 

    for (i = 0; i < up.length; i++) {
        $(up[i]).click(function() {        
            var temp = $(this).prev().html();
            var tempIsi = $(this).parent().next().html();
            var before = $(this).parent().prev().prev().children(".title")
            var beforeIsi = $(this).parent().prev()
            
            $(this).prev().html(before.html())
            $(before).html(temp);
            $(this).parent().next().html(beforeIsi.html())
            $(beforeIsi).html(tempIsi); 
        });
      } 

      for (i = 0; i < down.length; i++) {
        
        $(down[i]).click(function() {
            var temp = $(this).prev().prev().html();
            var tempIsi = $(this).parent().next().html()
            var next = $(this).parent().next().next().children(".title")
            var nextIsi = $(this).parent().next().next().next()

          
            $(this).prev().prev().html(next.html())
            $(next).html(temp);
            $(this).parent().next().html(nextIsi.html())
            $(nextIsi).html(tempIsi);
            
        });
      } 

 


});
