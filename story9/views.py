from django.shortcuts import render, redirect
from .forms import SignUpForm
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
# Create your views here.
def sign_up(request):
    form = SignUpForm
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Account was created for username '+ form.cleaned_data.get('username'))
            form = SignUpForm
    
    return render(request,"SignUp.html", {"title":"Sign Up","form":form})

def masuk(request):
    if request.method == 'POST':
        user = authenticate(username=request.POST['username'], password=request.POST['password'])
        if user is not None:
            login(request, user)
            return redirect("/home")
        else:
            messages.info(request, 'username or password incorect')
    # A backend authenticated the credentials
    
    return render(request,"Login.html", {"title":"Login"})

def keluar(request):
    logout(request)
    
    return redirect("/home")