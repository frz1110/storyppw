from django.test import TestCase, Client
from .views import sign_up, masuk, keluar
from django.urls import resolve
# Create your tests here.
class test_story_9(TestCase):
    def test_url_ada(self):
        response = Client().get("/SignUp")
        self.assertEqual(response.status_code, 200)
        response = Client().get("/Masuk")
        self.assertEqual(response.status_code, 200)
        
    def test_templates(self):
        response = Client().get('/SignUp')
        self.assertTemplateUsed(response, 'SignUp.html')
        html = response.content.decode('utf8')
        self.assertIn('Sign Up',html)
        response = Client().get('/Masuk')
        self.assertTemplateUsed(response, 'Login.html')
        response = Client().get('/Keluar')
        self.assertRedirects(response, "/home")

    def test_views_funct(self):
        found1 = resolve('/SignUp')
        self.assertEqual(found1.func, sign_up) 
        found1 = resolve('/Masuk')
        self.assertEqual(found1.func, masuk)  
        found1 = resolve('/Keluar')
        self.assertEqual(found1.func, keluar) 


