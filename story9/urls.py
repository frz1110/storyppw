from django.urls import path
from . import views

app_name = 'story9'

urlpatterns = [
    path('SignUp', views.sign_up, name='SignUp'), 
    path('Masuk', views.masuk, name='login'),   
    path('Keluar', views.keluar, name='logout'),   
]