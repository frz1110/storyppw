from django.test import TestCase, Client
from .models import Kegiatan, Orang
from .views import form_kegiatan,kegiatan, save_kegiatan
from django.urls import resolve
 

# Create your tests here.
class test_form_kegiatan(TestCase):
    def test_url(self):
        response = Client().get('/formKegiatan')
        self.assertEqual(response.status_code, 200)
        
    def test_templates(self):
        response = Client().get('/formKegiatan')
        self.assertTemplateUsed(response, 'kegiatan/formKegiatan.html')
        html = response.content.decode('utf8')
        self.assertIn('<input type="text" name="nama" placeholder="Nama Kegiatan" maxlength="20" required id="id_nama">', html)
        self.assertIn('<button  type="submit" class="btn btn-secondary"  style="background-color:#87a96b;" >Tambah</button>', html)

    def test_database(self):
        Kegiatan.objects.create(nama="PIKNIK")
        hitung_banyak_kegiatan = Kegiatan.objects.all().count()
        self.assertEqual(hitung_banyak_kegiatan, 1)

    def test_views_funct(self):
        found = resolve('/formKegiatan')
        self.assertEqual(found.func, form_kegiatan) 

        found1 = resolve('/saveKegiatan')
        self.assertEqual(found1.func, save_kegiatan) 

class test_form_daftar(TestCase):
    def test_url(self):
        response = Client().get('/formDaftar/1')
        self.assertEqual(response.status_code, 200)
        post = Client().get('/saveDaftar/1')
        self.assertRedirects(post,'/kegiatan')

    def test_templates(self):
        response = Client().get('/formDaftar/1')
        self.assertTemplateUsed(response, 'kegiatan/formDaftar.html')
        html = response.content.decode('utf8')
        self.assertIn('<input type="text" name="nama" placeholder="Nama Kamu" maxlength="20" required id="id_nama">', html)
        self.assertIn('<button  type="submit" class="btn btn-secondary"  style="background-color:#87a96b;" >Daftar</button>', html)

    def test_berhasil_daftar(self):
      
        kegiatan1 = Kegiatan.objects.create(nama="Piknik")
        orang1 = Orang.objects.create(nama="Farzana")
        orang1.kegiatan.add(kegiatan1)

        hitung_banyak_orang = Orang.objects.all().count()
        self.assertEquals(hitung_banyak_orang, 1)

        response = Client().get('/kegiatan')
        html = response.content.decode('utf8')
        self.assertIn(orang1.nama, html)

    
        

class test_kegiatan(TestCase):
    def test_url(self):
        response = Client().get('/kegiatan')
        self.assertEqual(response.status_code, 200)
	
    def test_templates(self):
        response = Client().get('/kegiatan')
        self.assertTemplateUsed(response, 'kegiatan/Kegiatan.html')

    def test_views_funct(self):
        found = resolve('/kegiatan')
        self.assertEqual(found.func, kegiatan) 
        

    
        
  