from django.shortcuts import render,redirect
from .forms import formKegiatan, formDaftar
from .models import Kegiatan, Orang
# Create your views here.

def form_kegiatan(request):
    form = formKegiatan
    response = {'title':'Form Kegiatan', 'form':form}
    return render(request, 'kegiatan/formKegiatan.html',response)

def save_kegiatan(request):
    form = formKegiatan(request.POST)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
    return redirect('/formKegiatan')

def form_daftar(request, pk):
    form = formDaftar
    response = {'title':'Form Daftar', 'form':form, 'pk':pk}
    return render(request, 'kegiatan/formDaftar.html',response)

def save_orang(request,pk):
    form = formDaftar(request.POST)
    if (form.is_valid() and request.method == 'POST'):
        orang = form.save()
        orang.kegiatan.add(Kegiatan.objects.get(pk=pk))
    return redirect('/kegiatan')

def kegiatan(request):
    list_orang = []
    kegiatan = Kegiatan.objects.all()
    for i in kegiatan:
        list_orang.append(i.orang_set.all())
    response = {'title':'Kegiatan', 'kegiatans' : kegiatan, 'list' : list_orang}
    return render(request, 'kegiatan/Kegiatan.html', response)