from django.urls import path
from . import views

app_name = 'story6'

urlpatterns = [
    path('formKegiatan', views.form_kegiatan, name='formKegiatan'),
    path('formDaftar/<int:pk>', views.form_daftar, name='formDaftar'),
    path('saveKegiatan', views.save_kegiatan, name='saveKegiatan'),
    path('saveDaftar/<int:pk>', views.save_orang, name='saveDaftar'),
    path('kegiatan', views.kegiatan, name='kegiatan'),
    
]