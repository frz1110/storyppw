from django import forms
from .models import Kegiatan, Orang

class formKegiatan(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = ['nama']

    input_attrs = {
        'type' :'text',
        'placeholder' : 'Nama Kegiatan'
    }

    nama = forms.CharField(label='', required=True, max_length = 20, widget = forms.TextInput(attrs=input_attrs))

class formDaftar(forms.ModelForm):
    class Meta:
        model = Orang
        fields = ['nama']

    input_attrs = {
        'type' :'text',
        'placeholder' : 'Nama Kamu'
    }

    nama = forms.CharField(label='', required=True, max_length = 20, widget = forms.TextInput(attrs=input_attrs))