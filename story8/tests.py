from django.test import TestCase, Client
from .views import book, data
from django.urls import resolve
# Create your tests here.
class test_story_8(TestCase):
    def test_url_ada(self):
        response = Client().get("/book")
        self.assertEqual(response.status_code, 200)
        response = Client().get("/data",{"q":"a"})
        self.assertEqual(response.status_code, 200)


    def test_templates(self):
        response = Client().get('/book')
        self.assertTemplateUsed(response, 'book.html')
        html = response.content.decode('utf8')
        self.assertIn('<input class="col-5" type="text" id="keyword" placeholder="cari buku...">', html)


    def test_views_funct(self):
        found1 = resolve('/book')
        self.assertEqual(found1.func, book)  

        found1 = resolve('/data')
        self.assertEqual(found1.func, data) 