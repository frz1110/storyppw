from django.urls import path
from . import views

app_name = 'story8'

urlpatterns = [
    path('book', views.book, name='book'),
    path('data', views.data, name='data'),
    
    
]