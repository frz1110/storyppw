from django.shortcuts import render
from django.http import JsonResponse
import requests
import json
# Create your views h
def book(request):
    return render(request,"book.html", {"title":"Books"})

def data(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    data = json.loads(requests.get(url).content)

    # just return a JsonResponse
    return JsonResponse(data)